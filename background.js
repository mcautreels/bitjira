var issueKeyPattern = /[A-Z][A-Z]+-[0-9]+/g
var issueInfoURI = "/rest/api/2/issue/";
var issueBrowseURI = "/browse/";
var settings = {};

jQuery(function($) {
    loadSettings(initialize);
});

var fail = function() {

}

var initialize = function() {
    $('.branch-header a').each(function() {
        var branchName = $(this).text();
        var issueKey = branchName.match(issueKeyPattern);

        if(issueKey != undefined && issueKey != ""){
            var url = settings.host + issueInfoURI + issueKey;

            $.ajax( {
                dataType: "json",
                url: url,
                success: parseIssueResult
            }).fail(fail);

            $(this).parent().attr('data-issuekey', issueKey);
        }
    });
}

var parseIssueResult = function(data) {
    var issueLink = settings.host + issueBrowseURI + data.key;
    var html = '<a title="' + data.fields.summary + '" class="issuekey-button aui-button aui-button-subtle" target="_blank" href="' + issueLink + '">' +
        '   <span class="aui-icon aui-icon-small aui-iconfont-jira">JIRA</span>' +
        '</a>';

    var branchLabel = $('[data-issuekey="' + data.key + '"]');
    if(settings.position == 0) {
        branchLabel.prepend(html).tipsy();
    } else {
        branchLabel.append(html).tipsy();
    }
}

var loadSettings = function(callback) {
    chrome.storage.sync.get({
        settings: settings
    }, function(items) {
        settings = items.settings;

        $.ajaxSetup({
            beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Basic " + btoa(settings.username + ":" + settings.password));
            }
        });

        callback();
    });
}
